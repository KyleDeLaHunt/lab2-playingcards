
//Lab Exercise 2 - Playing Cards\\
//Kyle De La Hunt\\

#include <iostream>
#include <conio.h>

using namespace std;



enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Heart, Spade, Diamond, Club
};

struct Card
{
	Rank Rank;
	Suit Suit;
};


// Prototypes
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);


int main()
{
	
	Card c1;
	c1.Rank = Ace;
	c1.Suit = Spade;

	Card c2;
	c2.Rank = Queen;
	c2.Suit = Club;

	PrintCard(HighCard(c1, c2));

	cout << "\nPress any key to quit..." << endl;
	_getch();
	return 0;
}

void PrintCard(Card card) {
	switch (card.Rank) {
	case Two: cout << "The Two of "; break;
	case Three: cout << "The Three of "; break;
	case Four: cout << "The Four of "; break;
	case Five: cout << "The Five of "; break;
	case Six: cout << "The Six of "; break;
	case Seven: cout << "The Seven of "; break;
	case Eight: cout << "The Eight of "; break;
	case Nine: cout << "The Nine of "; break;
	case Ten: cout << "The Ten of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	case Ace: cout << "The Ace of "; break;
	}
	switch (card.Suit) {
	case Heart: cout << "Hearts\n"; break;
	case Spade: cout << "Spades\n"; break;
	case Diamond: cout << "Diamonds\n"; break;
	case Club: cout << "Club\n"; break;
	}
}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank) return card1;

	return card2;
}